filebeat_repo:
  pkgrepo.managed:
{% if salt['grains.get']('os_family') == 'Debian' %}
    - humanname: Elastic PPA for 6.x packages
    - name: deb https://artifacts.elastic.co/packages/6.x/apt stable main
    - file: /etc/apt/sources.list.d/elastic-6.x.list
    - gpgcheck: 1
    - key_url: https://artifacts.elastic.co/GPG-KEY-elasticsearch
{% elif salt['grains.get']('os_family') == 'RedHat' %}
    - humanname: Elastic repository for 6.x packages
    - name: elastic-6.x
    - humanname: Elasticsearch repository for 2.x packages
    - baseurl: https://artifacts.elastic.co/packages/6.x/yum
    - gpgcheck: 1
    - gpgkey: https://artifacts.elastic.co/GPG-KEY-elasticsearch
{% endif %}
    - require_in:
      - pkg: filebeat.install
    - watch_in:
      - pkg: filebeat.install

filebeat.install:
  pkg.installed:
    - name: filebeat
