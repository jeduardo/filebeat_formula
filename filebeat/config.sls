{% for app in salt['pillar.get']('filebeat')  %}

filebeat.config-{{ app }}:
  file.managed:
    - name: /etc/filebeat/filebeat-{{ app }}.yml
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - source:
      - salt://filebeat/files/{{ app }}.jinja

{% endfor %}
